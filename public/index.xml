<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>CSDUMMI</title>
    <link>http://jorisgutjahr.eu/</link>
    <description>Recent content on CSDUMMI</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <copyright>2022 Joris &amp;ltCSDUMMI&amp;gt Gutjahr</copyright>
    <lastBuildDate>Thu, 22 Sep 2022 00:00:00 +0000</lastBuildDate><atom:link href="http://jorisgutjahr.eu/index.xml" rel="self" type="application/rss+xml" />
    <item>
      <title>Digital Democracy</title>
      <link>http://jorisgutjahr.eu/posts/digitaldemocracy/</link>
      <pubDate>Thu, 22 Sep 2022 00:00:00 +0000</pubDate>
      
      <guid>http://jorisgutjahr.eu/posts/digitaldemocracy/</guid>
      <description>Digital Democracy has been a chief passion of mine. When I started working on digital democracy I considered democracy a priori as the best form of social organisation. And while I still believe in the conclusion, I have since critically examined this assumption after I stopped working on DemNet.
DemNet My first project in the area of digital democracy was the democratic social network DemNet. The premise of this project was to create a network where the users govern the project and network themselves.</description>
      <content>&lt;p&gt;Digital Democracy has been a chief passion of mine. When I started working on digital democracy I considered democracy a priori as the best form of social organisation. And while I still believe in the conclusion, I have since critically examined this assumption after I stopped working on DemNet.&lt;/p&gt;
&lt;h2 id=&#34;demnet&#34;&gt;DemNet&lt;/h2&gt;
&lt;p&gt;My first project in the area of digital democracy was the democratic social network &lt;a href=&#34;https://codeberg.org/CSDUMMI/DemNet&#34;&gt;DemNet&lt;/a&gt;. The premise of this project was to create a network where the users govern the project and network themselves.&lt;/p&gt;
&lt;p&gt;This was achieved by, every weeks, holding an election in which every user could make proposals and vote on the proposals to adopt a single proposal. These proposal could contain both changes to the source code (in the form of PATCH files) and/or changes to the rules of the network.&lt;/p&gt;
&lt;p&gt;There were plans to add democratic moderation with elected moderators or juries. But these were never developed nor necessary, since the network was very small when I could no longer sustain development.&lt;/p&gt;
&lt;p&gt;This premise prompted certain foundational issues that are of interest for the advancement of digital democracy as well as some broader lessons for me on active software development with a community.&lt;/p&gt;
&lt;h3 id=&#34;humans-only-unique-franchise&#34;&gt;Humans-only unique Franchise&lt;/h3&gt;
&lt;p&gt;The first conceptual issue with a democratically governed social network is that we need to ensure some kind of equality of votes. If person can create an arbitrary number of accounts for themselves, they&amp;rsquo;d also be gain an arbitrary number of votes in any election. And that is not a fair system.&lt;/p&gt;
&lt;p&gt;There really only is one solution to this problem, that&amp;rsquo;ll be conceptually perfect. You&amp;rsquo;d have to require each user to provide us with some unique, uniform and permanently identifying information prior to sign up.&lt;/p&gt;
&lt;p&gt;Then we could automatically compare this information with all the other users already signed up and if any user was already signed up with that specific information, the sign up would be rejected.&lt;/p&gt;
&lt;p&gt;But what unique, uniform and permanently identifying information could we use without violating the privacy of our users? The data must uniform because it enables to only store an irreversible hash of the data, instead of the data itself, thus reducing the risk associated with a database leak. This disqualifies any kind of biometrics, because no two scans of biometric data results in the exact same bit pattern. And I&amp;rsquo;d not be confident in my abilities to secure the unscrambled biometric of the users. Thus all of these were disqualified from consideration.&lt;/p&gt;
&lt;p&gt;The other alternative is to use government provided identification, such as a tax identification number, passport number or the like. The problem with this approach is, that while uniform and unique as per the force of government, this data is not always permanent and no government provided identification will be universal. Using government identification is thus not practical without either violating the privacy or limiting the reach of the system at very basic level.&lt;/p&gt;
&lt;p&gt;We thus decided against a &amp;ldquo;perfect&amp;rdquo; solution and instead searched for one that could achieve the desired result relatively often. For as long as the network was running, this solution was to have every new user be manually reviewed and added by me to the network - relying on my memory not to add one person twice.&lt;/p&gt;
&lt;p&gt;This, of course, is impractical in a larger system and if I or anybody tries implement digital democracy again, I&amp;rsquo;d solve this problem by either having new users be voted in to receive voting rights coupled with a method of expelling by a vote again.&lt;/p&gt;
&lt;p&gt;This problem, when solved incorrectly, can threaten the security of the democratic process, either by double voting (i.e. one person having more than one vote) or by a too restrictive franchise (i.e. withholding from persons deserving of it).&lt;/p&gt;
&lt;h3 id=&#34;secrecy-of-elections&#34;&gt;Secrecy of Elections&lt;/h3&gt;
&lt;p&gt;One of the hardest problems in computing is keeping some data secret. It is even more problematic, when the data is not just supposed to be kept secret from someone but from anyone, as is the case with internet voting.&lt;/p&gt;
&lt;p&gt;The basic requirement on elections is that nobody will be able trace the ballot to the voter that cast that ballot. In a physical election, this is achieved through ballot boxes. But there is no real digital equivalent to these ballot boxes.&lt;/p&gt;
&lt;p&gt;Even if we setup a &amp;ldquo;ballot box&amp;rdquo; service, that received voters, ballot pairs and spit out a list of voters and a list of ballots, entirely shuffled around to prevent any association between the two, there&amp;rsquo;d still have to be somebody administrating or owning that &amp;ldquo;ballot box&amp;rdquo; service. And there is no guarantee that they might still be able to associate voters to their ballots. Perhaps they keep logs of incoming requests, perhaps their memory is being monitored, perhaps they run the service in the cloud or on a VPS and their provider makes regular backups of the entire state of the VM (including RAM).&lt;/p&gt;
&lt;p&gt;The closest we may get to a digital ballot box at the moment is through a combination of mix network routing and traceable ring signatures.&lt;/p&gt;
&lt;p&gt;Mix networks, such as TOR, allow us to obscure the sender of a message. Sending a ballot through a mix net should make it infeasible for either the receiving server or a third-party adversary to detect the voter of the ballot.&lt;/p&gt;
&lt;p&gt;Ensuring that a ballot was issued by a legitimate voter who has not voted before, while not identifying the voter is a difficult problem.&lt;/p&gt;
&lt;p&gt;I pondered several solutions:&lt;/p&gt;
&lt;ol&gt;
&lt;li&gt;Traceable Ring Signatures&lt;/li&gt;
&lt;li&gt;Voter Keys&lt;/li&gt;
&lt;li&gt;&lt;/li&gt;
&lt;/ol&gt;
&lt;h3 id=&#34;heading&#34;&gt;&lt;/h3&gt;
</content>
    </item>
    
  </channel>
</rss>
