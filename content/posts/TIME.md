---
title: "Mutating Secrets: A double spend resistent implementation with a trusted timestamping authority"
author: CSDUMMI
date: 2022-01-04
language: en
---

I have to thank the Fediverse before writing up another post on this subject. The kind of discussions I took part in during the last
few days, were fascinating and I cannot imagine that they would have happened so constructively and with similar civility on another network.[^1].

During these discussions I was able to detail my idea for a mutating secrets based currency more clearly, while being raised to an issue I'll go in to soon
and being alerted to the existence of other (quite similar proposals) like Jessica Tallon's idea for a pebble bank[^2].

During this discussion I was made aware of a problem with my proposal - a problem that I pretty much hand waived away in my last post on the subject.

#### Double Spending in an asychronous network
Caleb (@cjd@mastodon.social) got me to ponder this scenario:
> Caleb sends csdummi and silverpill the same coin simultaneously. Both csdummi and silverpill claim the coin. After they synchronize who owns the coin? [^3]

This problem is very important, because we cannot just return the coin to Caleb because it would mean Caleb could get any
spend coin back by just submiting a claim themselves after a trade with that coin has already happened.

So there has to be a decision made between these two claims to decide who is the rightful owner of this new coin.
And the means using timestamps.

I identified two different types of timestamps (and a combination of the two - that is irrelevant here)[^4]:
1. Forward Timestamp: a timestamp that can only be created anytime after a certain event.
2. Backward Timestamp: a timestmap that can only be created anytime before a certain event.

Creating a forward timestamp is trivial - a document just needs to refer to data that could not have been know
before a certain time. For example if I refer to a block hash of Bitcoin in this document I cannot have created
this document before that block was created [^5].

Creating a backward timestamp is more complicated. Using Bitcoin again I could create a backward timestamp by
adding some data or hash of some data to the Blockchain in a transaction. This data could only have been created before it was added
to the Blockchain. Making it backwards timestamped.

#### Using Backwards Timestamps to resolve claims disputes.
The rule I want to implement to resolve claims disputes is
the following: *The first claim wins*.
I.e. the claim that was made first is the claim that gets the coin.

This is the only reasonable rule, because the inverse: *the last claim wins*, is clearly neither secure nor reliable.

In order to implement this rule each claim needs to be accompanied by a
backwards timestamp (because a forward timestamp would implement said insecure inverse rule, because new claims could just choose timestamps that lie ever further in the past).

Given these backwards timestamps the disputing claims can be ordered and the
claim with the earliest backward timestamp wins the dispute.

#### How Backwards Timestamps "solve" the double spending problem
In the above named scenario where Caleb tries to double spend the coin,
both csdummi and silverpill would try to get a backward timestamp and
the lucky one getting the earlier timestamp wins the coin.

This still leaves the problem of how csdummi and silverpill
get to know of their disputed coin, although that dispute has already
been solved.

And solving that issue is what I'll hand waive again with the following naive and pratical rule:
> Both csdummi and silverpill wait a randomized timeout before verifying their coins which means they'll be able to synchronize each others claims.

If they cannot synchronize each others claims within this timeout, that'll be the risk they take by using this system.

And in fast network (i.e. ones that don't take more than 10s to send a message across the globe) this risk I think can
be reduced to such a level that most people can take that risk especially over such small amounts as 1 coin.

Be aware that for larger transactions than 1 coin this claim-timeout-verify process has to be repeated for every single
coin which increases the likelyhood of larger scale double spending being detected.

And double spending of smaller amounts is going to be detected eventually and will probably be
a risk low enough to be comparable with the risk a shop takes when not checking every single coin
and bill they receive for counterfit.

And if a person or business does not want to take that risk they can always increase the timeout and reduce that risk.

#### Using a trusted timestamp authority (TSA) to implement a mutating secrets currency
All that I discussed above relies on a mechanism to create secure backward timestamps.
And I described how backward timestamps can be created using a Blockchain.

But that is not the only way to implement backward timestamps and an existing alternative
would be to use a trusted timestamp authority (TSA).

A TSA is a server running on the internet that receives requests to timestamp some data
and returns a signature of that data and a timestamp based on their local clock.

This signature, data, timestamp tuple could then be used to proof the time a document was created before
to anyone who trusts that server to operate non-maliciously.

That this system is centralised is obvious. But this is not necessarily a problem for a mutating secrets
currency scheme.

Because all transactions in such a scheme are private (i.e. their spender, amount and possibly receiver are hidden when the transaction is made),
the timestamp authority cannot deny anyone a timestamp based on such data.

The only thing a timestamp authority can do is categorically not make timestamps for anyone or make timestamps for everyone - as long as they don't require
any additional data to make a timestamp which should be a serious red flag for anyone using them.

Thus what a mutating secrets currency using a trusted TSA for timestamping is centralising is availability of their currency
but not integrity, confidentiality, privacy or anonymity.

And it is far easier to setup a new currency using a trusted TSA than using a Blockchain - because setting up a Blockchain is costly and
is often not decentralised anyway, if you and your associates are the only people running miners, validators or whatever you want to call
those nodes that make the network operate.

Another thing to consider here is how modularized the timestamping and timestamp verification can be implemented.
If the verification of a trusted TSA timestamp is implemented in a modular way, it'd make it
possible to switch to a Blockchain for backward timestamping later on after the currency has
grown and been adopted by so many people that you no longer want to depend on a single server
for your timestamps.

#### Conclusion
Thanks to anyone reading this post and anyone who participated in the thread on the Fediverse about my
previous proposal. I hope some of the concerns raised during that discussion were addressed in this post.

In hopes of review feedback and constructive critique,

CSDUMMI

[^1]: Thanks to anyone who took part in this thread: https://norden.social/web/statuses/107549142172900602
[^2]: "Digital cash in an asynchronous environment", Jessica Tallon ([https://spritelyproject.org/news/pebble-bank.html](https://spritelyproject.org/news/pebble-bank.html))
[^3]: [https://mastodon.social/@cjd/107554282451070679](https://mastodon.social/@cjd/107554282451070679)
[^4]: [https://norden.social/@csddumi/107554415248627056](https://norden.social/@csddumi/107554415248627056)
[^5]: 000000000000000000022f466e70a3d6aad0eb0f483e7b42ef3e9c09d4556aab, height: 717134, timestamp: 2022-01-04 09:53 I could not have had that data on the 2022-01-03. But I can have that data anytime after the 2022-01-04.
