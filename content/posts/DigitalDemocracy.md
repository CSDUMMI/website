---
title: Digital Democracy
subtitle: DemNet
date: 2022-09-22
toc: true
draft: true
---

Digital Democracy has been a chief passion of mine. When I started working on digital democracy I considered democracy a priori as the best form of social organisation. And while I still believe in the conclusion, I have since critically examined this assumption after I stopped working on DemNet.

## DemNet
My first project in the area of digital democracy was the democratic social network [DemNet](https://codeberg.org/CSDUMMI/DemNet). The premise of this project was to create a network where the users govern the project and network themselves.

This was achieved by, every weeks, holding an election in which every user could make proposals and vote on the proposals to adopt a single proposal. These proposal could contain both changes to the source code (in the form of PATCH files) and/or changes to the rules of the network.

There were plans to add democratic moderation with elected moderators or juries. But these were never developed nor necessary, since the network was very small when I could no longer sustain development.

This premise prompted certain foundational issues that are of interest for the advancement of digital democracy as well as some broader lessons for me on active software development with a community.

### Humans-only unique Franchise
The first conceptual issue with a democratically governed social network is that we need to ensure some kind of equality of votes. If person can create an arbitrary number of accounts for themselves, they'd also be gain an arbitrary number of votes in any election. And that is not a fair system.

There really only is one solution to this problem, that'll be conceptually perfect. You'd have to require each user to provide us with some unique, uniform and permanently identifying information prior to sign up.

Then we could automatically compare this information with all the other users already signed up and if any user was already signed up with that specific information, the sign up would be rejected.

But what unique, uniform and permanently identifying information could we use without violating the privacy of our users? The data must uniform because it enables to only store an irreversible hash of the data, instead of the data itself, thus reducing the risk associated with a database leak. This disqualifies any kind of biometrics, because no two scans of biometric data results in the exact same bit pattern. And I'd not be confident in my abilities to secure the unscrambled biometric of the users. Thus all of these were disqualified from consideration.

The other alternative is to use government provided identification, such as a tax identification number, passport number or the like. The problem with this approach is, that while uniform and unique as per the force of government, this data is not always permanent and no government provided identification will be universal. Using government identification is thus not practical without either violating the privacy or limiting the reach of the system at very basic level.

We thus decided against a "perfect" solution and instead searched for one that could achieve the desired result relatively often. For as long as the network was running, this solution was to have every new user be manually reviewed and added by me to the network - relying on my memory not to add one person twice.

This, of course, is impractical in a larger system and if I or anybody tries implement digital democracy again, I'd solve this problem by either having new users be voted in to receive voting rights coupled with a method of expelling by a vote again.

This problem, when solved incorrectly, can threaten the security of the democratic process, either by double voting (i.e. one person having more than one vote) or by a too restrictive franchise (i.e. withholding from persons deserving of it).

### Secrecy of Elections
One of the hardest problems in computing is keeping some data secret. It is even more problematic, when the data is not just supposed to be kept secret from someone but from anyone, as is the case with internet voting.

The basic requirement on elections is that nobody will be able trace the ballot to the voter that cast that ballot. In a physical election, this is achieved through ballot boxes. But there is no real digital equivalent to these ballot boxes.

Even if we setup a "ballot box" service, that received voters, ballot pairs and spit out a list of voters and a list of ballots, entirely shuffled around to prevent any association between the two, there'd still have to be somebody administrating or owning that "ballot box" service. And there is no guarantee that they might still be able to associate voters to their ballots. Perhaps they keep logs of incoming requests, perhaps their memory is being monitored, perhaps they run the service in the cloud or on a VPS and their provider makes regular backups of the entire state of the VM (including RAM).

The closest we may get to a digital ballot box at the moment is through a combination of mix network routing and traceable ring signatures.

Mix networks, such as TOR, allow us to obscure the sender of a message. Sending a ballot through a mix net should make it infeasible for either the receiving server or a third-party adversary to detect the voter of the ballot.

Ensuring that a ballot was issued by a legitimate voter who has not voted before, while not identifying the voter is a difficult problem.

Once again there is a "perfect" solution and a practical one. The "perfect" solution would be to deploy a [one-time traceable ring signature](https://link.springer.com/chapter/10.1007/978-3-030-88428-4_24). A traceable ring signature is a signature can only be created with a private key in a ring of key pairs on an issue (in our case, a unique election id). If multiple signatures are created with the same private key on the same issue (election), the private key used for creating the signature will be exposed.

These are very desirable properties in principle for a voting system. If you have a ballot signed by a traceable ring signature algorithm and a ring of either all or parts of the franchise, you could guarantee that a ballot received belongs to a voter in the franchise and that that voter has not issued a different ballot already. The reason a one-time traceable ring signature and not a traceable ring signature should be used, is to prevent tracking across several elections of the voting behavior.

Why is this system not practical? We are lacking a reliable implementation of a  traceable ring signature algorithm. The article linked above was published in 2021 and the academic discourse on the topic is still very active. It is to be expected that it'll take a few years until a secure, audited and reliable implementation of traceable ring signatures (let alone one-time traceable ring signatures) is available. It appears that [every](https://link.springer.com/chapter/10.1007/978-3-540-71677-8_13?error=cookies_not_supported&code=8d61ae3b-348f-419e-9974-c5211a5617b5) [few](https://link.springer.com/chapter/10.1007/978-981-13-1483-4_5?error=cookies_not_supported&code=bf1a6c99-e543-4d1d-a09f-8d89547f9d92) [years](https://link.springer.com/article/10.1007/s10623-021-00863-x) a [new](https://web.archive.org/web/20190624143658/https://cryptonote.org/whitepaper.pdf) [scheme](https://www.hindawi.com/journals/scn/2022/3938321/) in the area of ring signatures is suggested.

Due to this problem, we'll have to wait for someone to implement a secure traceable ring signature algorithm or do it ourselves.

Because of this fact, DemNet did not provide secure secret elections using ring signatures. Or any degree of protection against the server operator. If I, as the server operator, decided to log the ballot, voter pairs, I could at any point have done so. Of course, I was not interested in doing this during the term of this project. But even still, it was difficult to prevent me from knowing accidentally.

I had to be careful to remove both incrementing primary keys and restrain myself from printing the participant and ballot records during the election, because both could violate the secrecy of the election. Incrementing primary keys on the ballot and participant record are an indirect link between the two. And based on the changes in participant and ballot records you can either make an educated guess or have direct evidence of a ballot-participant association (if the change were one new ballot and participant, it is evident to whom ballot belongs).

After operating DemNet for one and a half years, I conclude that I-Voting is not a case where you can *trust* somebody not to violate the secrecy of an election, because it is very hard to permanently remove any link on the side of the election server after receiving it. Instead it should be required that a valid ballot cannot be linked to it's voter after leaving the device of said voter.

### Intuitive Voting Interface
A frequent problem of first time voters was the voting interface. As DemNet used ranked-choice voting, it was already a very unfamiliar voting method. Instead having to select a single option, voters had to rank all options according to preference - which was implemented using a drag and drop interface.

Due to the lack of a confirmation dialog, people clicked "vote" without changing the ballot. I ultimately instructed the voters myself and added a note on the voting page - although I never added a confirmation dialog.

It is very important that an I-Voting system must have an intuitive user interface and strong error protection, such as a confirmation dialog.

### Electoral Systems
I already mentioned that DemNet used [instant runoff voting](https://en.wikipedia.org/wiki/Instant-runoff_voting), a variant of ranked-choice voting, with a single result that is supposed to avoid tactical voting. But several questions arose that I was not able to definitively answer from my research on IRV.

#### Ties
To explain very briefly what IRV does:
1. All ballots are a list from best to least objectionable option chosen.
2. If an absolute majority of voters (>50%) agree on first option, that option wins.
3. Otherwise the least favourite option among first options is removed from the election.
4. The voters with the removed option as their first choice, are treated like their second choice was their first choice from now on.
5. Repeat from step 2 until a winner is found.

There are two possible tie situation in this algorithm:
1. Two options could have 50% of the first option votes.
2. Two options could have the exact same least amount of votes.

In both cases IRV is rather ambiguous on how to handle these two scenarios, mostly because the probability of either tie coming about and being of consequence to the result in elections with thousands or millions of participants is negligible. I had to actively contemplate both situation during the implementation of the voting method.

In the end I decided that in the first case, no result could be had.If two options have exactly equal votes, I could not declare one of these options a winner over the other.
When an election occurred where two proposal with the same name were proposed, it actually resulted this tie and we had to wait until the next election cycle to make another change to the source code.

I also decided to neglect the second type of tie for the sake simplicity. When the two least popular options have equal votes, one of them will be chosen and which one is determined by Python's `sorted` function.
The source code for the `count` method used by DemNet is actually only 59 lines long and can be found [here](https://codeberg.org/CSDUMMI/DemNet/src/branch/master/Server/election.py).

#### Abstention and Peer-to-Peer
A second question troubled me was the problem of abstentions or ballots running out of options. DemNet, as an authoritative centralized election server, has the luxury of deciding in favour or against counting abstentions.

In a peer-to-peer system this luxury would not exist. A peer is never guaranteed knowledge of all ballots cast across the network. They can thus not declare a winner, unless no further ballots would modify that result. If we assume that the peer has knowledge of all eligible voters and of their number, we can construct an electoral system that fulfills this criteria.

An electoral system that satisfies this criteria must count abstentions as opposing the declaration of any winner and not declare a winner before majority is reached

