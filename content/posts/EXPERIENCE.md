---
title: My Experience
date: 2022-11-25
description: What I've done, what I'm doing.
---
I've been working  on computing since 2015. While the first language I tried was Python, the first language I understood was Java.

I've since learned quite a few other procedural, object oriented and functional languages, such as C, JavaScript, Python, Haskell and Elm.

I've consistently published my projects, small and large, on [GitHub](https://github.com/CSDUMMI), [GitLab](https://gitlab.com/CSDUMMI) and most recently also on [Codeberg](https://codeberg.org/CSDUMMI). On all of these platforms I use the alias CSDUMMI consistently.

I support and believe in free software and try to develop sustainable methods for developing free software at the [Social Coding Movement](https://coding.social).

My largest project yet has been DemNet. The goal of this project was to create a social network governed and owned in some sense by it's users. You can view the project's [source code](https://gitlab.com/CSDUMMI/DemNet) but it is no longer operational. I developed and administered this project from April 2020 to September 2021. I had to stop running this project on [democraticnet.de](https://democraticnet.de) after a year because I was no longer able to continue development and administration alone.

Since I lacked a personal project in the subsequent months, I had the opportunity to finally contribute more effort towards a free software project. Which I did by adding to the documentation of [OrbitDB](https://github.com/orbitdb/field-manual/pull/132) to familiarize myself with the project, until I was able to modify the source code as well - as I did in this [PR](https://github.com/orbitdb/orbit-db-access-controllers/pull/66) among many others.

At the end of the summer of 2022, I began working with Serge from Babka on the [Babka](https://blog.babka.social/) project, where I worked on free software projects as well as the fork of [Mastodon](https://gitlab.com/babka_net/mastodon-babka) used by Babka.


