---
title: SSO and Mastodon
description: My first Mastodon contributions to support SSO.
author: CSDUMMI
date: 2023-03-18
category: Babka
tags:
- Babka
- Mastodon
- Security
- SSO
- Omniauth
- Devise
- FOSS
- dependency
- Gem
---
Did you know that Mastodon supports Single-Sign-On? Most Mastodon users will not, because most instances do not make use of this great feature.

A few services that use this feature include:
- [social.edu.nl](https://social.edu.nl/about) allows users to login through Dutch education universities and higher education institutes.
- [toot.thoughtworks.com](https://toot.thoughtworks.com/about) is an instance setup by the Thoughtworks company for it's employees and using the company's Okta identity provider.

The difference between these and [babka.social](https://babka.social) is that Babka has set up it's own identity provider, not because it wants to restrict access to a specific existing user base, but because it gives Babka the ability to add further services using the same credentials and user profiles in the future.

For the last few months I worked to fix several bugs with the Mastodon SSO integration using the OpenID Connect protocol. This resulted in several [pull requests](https://github.com/mastodon/mastodon/pulls?q=is%3Apr+author%3ACSDUMMI+is%3Amerged+) to the Mastodon repository, all of whom have by now been merged. Throughout this time I learned a lot about the way that Mastodon implements SSO and what difficulties this brings with it.

In general I think that SSO was implement in Mastodon with a focus on convenience to implement it first and audit-ability and sustainability second. I'm absolutely certain that this is not just the case for this one feature or even just this one project.

### History of SSO in Mastodon
Mastodon supports three Single-Sign-On protocols (ignoring LDAP): CAS, SAML and OpenID Connect.
Support was first added for CAS and SAML in [2018](https://github.com/mastodon/mastodon/pull/6425) after a discussion dating back to [2017](https://github.com/mastodon/mastodon/pull/3148).
OpenID Connect was only added in [2022](https://github.com/mastodon/mastodon/pull/16221) almost a year after the PR introducing it had been opened.

This long development and testing time should be expected from a change where security is an obvious concern. Especially while the OpenID Connect PR was still waiting to be merged, several instances already tested the changes through forks. This is commendable behavior when introducing a new feature like this.

### How does SSO in Mastodon work?
Mastodon does not implement any of these protocols itself and instead uses a library called [`Omniauth`](https://github.com/omniauth/omniauth) that allows for the integration of various SSO protocols with the [`devise`](https://github.com/heartcombo/devise) library used for authentication in Mastodon (and many other Ruby on Rails apps).

Internally the `omniauth` library is itself not a monolith, instead only providing the general structure to execute various `strategies` to support different SSO protocols or even individual SSO providers. These strategies exist in separate libraries, often independently maintained and occasionally forked.

There are strategies for SAML ([`omniauth-saml`](https://github.com/omniauth/omniauth-saml)), CAS ([`omniauth-cas`](https://github.com/dlindahl/omniauth-cas)), OpenID Connect ([`omniauth_openid_connect`](https://github.com/omniauth/omniauth_openid_connect)) and many others, often maintained independently from each other.

Only recently have some of these been brought under the Omniauth Community organization on [Github](https://github.com/omniauth) and until February 13th the main Mastodon branch used a fork of the `omniauth_openid_connect` strategy, called the `gitlab_omniauth_openid_connect` gem, which is the dependency I mostly worked with throughout my bug fixes.

This approach has the advantage that Mastodon developers do not have to implement or really know about the details of the different protocols they are supporting. Something they [freely admit to](https://github.com/mastodon/mastodon/pull/16221#pullrequestreview-831792745) and which is to be expected from a software implementing such a vast variety of protocols and features as Mastodon.

But it also means that there exists a wide variety of security-related dependencies with an uncertain maintenance record. I have rendered a dependency graph of only this part of Mastodon:

```mermaid
classDiagram
    Mastodon ..> Omniauth
    Mastodon ..> omniauth_openid_connect
    Mastodon ..> omniauth_cas
    Mastodon ..> omniauth_saml
    Mastodon ..> devise

    Omniauth .. omniauth_openid_connect
    Omniauth .. omniauth_cas
    Omniauth .. omniauth_saml

    Omniauth .. devise
```
As you can see there are four different independently maintained and interdependent dependencies of Mastodon just to provide this one feature.

From a security standpoint, hijacking any one of these packages (especially if they are updated without a proper review) could lead to a security vulnerability in Mastodon SSO support.

Under less malicious assumptions, this situation raises concerns about sustainability and maintainability. Who is responsible for a given library? What library is responsible for a given bug and where should a bug in Mastodon be reported to and solved in?

None of these questions are trivially answered. The general rule applied to the last question appears to be that Mastodon, as the software directly facing non-technical users, is responsible for receiving bug reports, analyzing them and then potentially forwarding them to the responsible dependency.[^1]

Partly because of this division of responsibility, auditing the code causing a specific bug and fixing is much harder. Different dependencies have different degrees of documentation, different formats, different teams of maintainers (or the lack thereof).

I'll illustrate these problems on the example of my two PRs: ["Redirect users to SLO at the IdP after logging them out of Mastodon/SSO No-logout"](https://github.com/mastodon/mastodon/pull/24020) and ["Prefer the stored location as after_sign_in_path in Omniauth Callback Controller/SSO No-App"](https://github.com/mastodon/mastodon/pull/24073)

### SSO No-Logout
This bug was identified very early on during the operations of the babka.social instance. When a user signed out of the Mastodon instance and then clicked on the Sign in button again, they'd immediately be signed in again to the same account without being prompted to authenticate themselves again.


I opened an issue for this [bug](https://github.com/mastodon/mastodon/issues/21572). While the discussion on this issue was on-going, I researched the dependencies involved in this bug and found a bug fix by asking these questions:
1. How was logging out of the Identity Provider from Mastodon supposed to work? Answer: By redirecting the user to the `end_session_uri` after logging them out of Mastodon. I'll call this process "Single-Logout" or SLO.
2. What dependency is responsible for the OpenID Connect strategy in Mastodon? Answer: At the time this was [gitlab_omniauth_openid_connect](https://gitlab.com/gitlab-org/ruby/gems/gitlab-omniauth-openid-connect).
3. How does this dependency implement SLO? Answer: Omniauth is a middleware and when the `current_path` matches a certain regex it triggers a logout.
4. What is this regex? Answer: `%r{\A#{Regexp.quote(request_path)}(/logout)}`
5. Mastodon logout paths don't end in a `/logout`. Do we need to configure this hard-coded string to be `/auth/sign_out`? No, but I proposed a PR with this change anyway - so you can now configure it in the library, although we don't need it for Mastodon.[^2]
6. What is `request_path`? Answer: For Mastodon it is `/auth/auth/openid_connect`.
7. Where can I redirect the user to `/auth/auth/openid_connect/logout` in Mastodon? Answer: In the [`after_sign_out_path_for`](https://github.com/mastodon/mastodon/blob/717683d1c39d2fe85d1cc3f5223e1f4cf43f1900/app/controllers/application_controller.rb#L63) method of the `application_controller`.
8. Does changing this to `/auth/auth/openid_connect/logout` solve this bug? Answer: Yes.
9. Under what configuration should this function trigger OpenID Connect SLO? Answer: Only in environments where OpenID Connect is enabled and where users can only use Omniauth to sign in to Mastodon, so both `OIDC_ENABLED` and `OMNIAUTH_ONLY` must be `true`.

This process might look straight-forward and condensed now, but as you can see in question 5, I did occasionally go in the wrong direction and since the directory structure of a rails application is a mystery to the uninitiated, I also had to had luck to notice the `after_sign_out_path_for` method of the `application_controller` in step 7.

Part of the reason for the above mentioned dependency graph was that Omniauth should be ["an extremely low-touch library"](https://github.com/omniauth/omniauth#integrating-omniauth-into-your-application), but throughout this bug fix that was far from my experience with it. I didn't just have to read the code of both Omniauth and the omniauth_openid_connect strategy, I also had to fork and experiment with them to answer some of the questions mentioned above. It is "low-touch" only when integrating the library into your application, but once it was necessary to fix a bug in the Mastodon code, I had to research the Omniauth and strategy code to figure out what actually happens. For this library to be "low-touch" there should be more documentation of both the internal, strategy and external [APIs](https://rubydoc.info/github/intridea/omniauth/master/OmniAuth/Strategy#request_path-instance_method) then there currently is.

Another thing to note about this bug is that while my PR has now been merged into Mastodon and is part of the Mastodon v4.1.1 release, I've not been able to merge a related PR to this in the [omniauth_openid_connect](https://github.com/omniauth/omniauth_openid_connect/pull/149) library - due to which certain Identity Providers might only log-out a user of their session on the identity provider without redirecting them back to Mastodon. This is not a critical problem, but if there had actually been any change to the omniauth_openid_connect gem that was required for SLO to be enabled on Mastodon, this bug would perhaps still be open today.

This was a big worry of mine while working on this and the other SSO issues - I only knew at the very end where I'd have to contribute to fix this issue and if I hadn't been lucky that all issues could be solved inside of Mastodon itself, these bugs could very well still exist in Mastodon today or in a few months for that matter.

The second major bug I fixed with the Mastodon SSO integration relates to using mobile apps or third-party web apps with Mastodon.[^3]

###  SSO No-App
This bug has been filed as an [issue](https://github.com/mastodon/mastodon/issues/18481) to Mastodon all the way back in May 2022. The bug was that when a user logged in through SSO they'd always be redirected to the Mastodon homepage, which was especially annoying for the users of mobile phone apps. These apps open a Web View of the `/oauth/authorize` path to grant themselves an access token to the user's account, from there a user is then redirected to the SSO provider to authenticate themselves and afterwards, instead of going back to `/oauth/authorize`, they are shown the Mastodon Web UI. While the issue had already been open for half a year when I joined the discussion, it had not progressed far beyond a description of the actual problem at that point.

You can see all the theories I developed and tested as to the cause and fix of this bug in the issue itself. I'll only describe the actual cause here.

Let's look at a snippet from the Omniauth Callback controller - this code is executed after a user has authenticated themselves with the identity provider and been redirected back to Mastodon:
```ruby
 def self.provides_callback_for(provider)
    define_method provider do
      @user = User.find_for_oauth(request.env['omniauth.auth'], current_user)

      if @user.persisted?
        LoginActivity.create(
          user: @user,
          success: true,
          authentication_method: :omniauth,
          provider: provider,
          ip: request.remote_ip,
          user_agent: request.user_agent
        )

        sign_in_and_redirect @user, event: :authentication
        label = Devise.omniauth_configs[provider]&.strategy&.display_name.presence || I18n.t("auth.providers.#{provider}", default: provider.to_s.chomp('_oauth2').capitalize)
        set_flash_message(:notice, :success, kind: label) if is_navigational_format?
      else
        session["devise.#{provider}_data"] = request.env['omniauth.auth']
        redirect_to new_user_registration_url
      end
    end
  end
```

As you can see a user is redirected after sign in using the `sign_in_and_redirect` function for which the documentation of devise states:
> Sign in a user and tries to redirect first to the stored location and then to the url specified by after_sign_in_path_for.[^4]

Reading this I (and the person implementing this function) assumed that the  `sign_in_and_redirect` method would only use the `after_sign_in_path_for` method after it checked the location stored in the session.

But the actual `sign_in_and_redirect` method looks like this:

```ruby
def sign_in_and_redirect(resource_or_scope, *args)
    options  = args.extract_options!
    scope    = Devise::Mapping.find_scope!(resource_or_scope)
    resource = args.last || resource_or_scope
    sign_in(scope, resource, options)
    redirect_to after_sign_in_path_for(resource)
end
```

Only the last line in this function matters. Instead of using the store location (a value in the session storing the last location a user visited), it always redirects users to the location returned by `after_sign_in_path_for`.

After realizing this the bug fix was simple. The Omniauth Callback Controller was overwriting this `after_sign_in_path_for` method, but ignored (probably because of the documentation of `sign_in_and_redirect`) the stored location.

Thus this whole issue, open for almost eight months, was fixed by a one-liner in Mastodon.

### Conclusion
In the end I'm happy that I was able to contribute to the Mastodon project and one of it's dependencies through my work on Babka. I learned that I need to apply a scientific method to bug fixing, falsifying hypothesis as to the cause of a bug until I couldn't falsify it anymore. At which point I'd be very close to the cause, but maybe not the fix of a bug.

And I learned that the primary source on how a piece of software operates is it's source code. All other documents about the software are just potentially flawed interpretations. I'll still use documentation, quick starters and examples, but I think I should never be hesitant to look at the source code of the software I'm actually using.

Especially now that I review my work on the No-App issue, I'm questioning if Mastodon is actually the right place to fix that bug. The documentation of that devise function will have been used by more projects than Mastodon, where the developers too didn't bother checking the source code to verify the document was accurate. In a situation where there are many dependencies combined to achieve a feature, the lines of responsibility are sometimes blurry.


[^1]: This can be illustrated by looking at the number of issues filed on Mastodon compared with devise at the time of writing. [9136](https://github.com/mastodon/mastodon/issues?q=is%3Aissue+) have been filed on Mastodon and only 40% percent of these issues have yet been closed. On Devise there are only [4045](https://github.com/heartcombo/devise/issues?q=is%3Aissue) issues in total of which only 3% are still open.
[^2]: The reason I considered this change necessary is because I thought that `request_path` (an undocumented value defined by Omniauth) was actually the path of the request currently processed (it's not) and that the Regex notation used, that I considered the possibility that it was expressing a comparison between the current request's path and `/logout`. This was not the case and so I had to find the value of the `request_path` variable (which I eventually did by just logging the value on my staging server).
[^3]: An example of an alternative Web UI is [pinafore.social](https://pinafore.social), which was a very useful tool for me when debugging this problem, because I could test my changes on a desktop and use the browser's developer tools. Thanks Pinafore.
[^4]: https://www.rubydoc.info/github/heartcombo/devise/Devise/Controllers/Helpers:sign_in_and_redirect#

