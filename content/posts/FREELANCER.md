---
title: I'm a freelance FOSS Programmer
description: I have experience contributing a wide-variety of FOSS projects. If you want me to contribute to FOSS projects or create new ones, you can hire me.
date: 2023-03-17
---

Hello,

I'm a FOSS programmer. During the last six months I've worked on [babka.social](https://babka.social) and [ActivityColander](https://gitlab.com/babka_net/activitycolander) as a freelancer for [@emacsen/Serge](https://emacsen.net)
As I've now setup myself to work as a freelancer and have a lot of free time on my hand for the next half year, I'm available if you want to have some FOSS software developed, whether developing something new, fork an existing project or patch and merge changes to an existing project. I can be contacted at [me@jorisgutjahr.eu](mailto:me@jorisgutjahr.eu) or through the handles in the footer.


My experience includes:
- Developing PRs for [Mastodon](https://github.com/mastodon/mastodon/pulls?q=is%3Apr+author%3ACSDUMMI) from the patches I created on the [`Mastodon +Babka`](https://gitlab.com/babka_net/mastodon-babka) for the [babka.social](https://babka.social) instance.
- Developing PRs for [`omniauth_openid_connect`](https://github.com/omniauth/omniauth_openid_connect/pulls?q=is%3Apr+author%3ACSDUMMI) as this is a dependency of Mastodon.
- Developing [ActivityColander](https://gitlab.com/babka_net/activitycolander): a spam-filter for ActivityPub.
- Developing the python packages [pynodeinfo](https://pypi.org/project/pynodeinfo/) and [maintain-website-tool](https://pypi.org/project/maintain-website-tool/).
- Contributing to [OrbitDB](https://github.com/orbitdb/orbit-db/pulls?q=is%3Apr+author%3ACSDUMMI) and the [OrbitDB Field Manual](https://github.com/orbitdb/field-manual/pulls?q=is%3Apr+author%3ACSDUMMI).
- Developing the [DemNet](https://gitlab.com/CSDUMMI/DemNet) social network prototype.

