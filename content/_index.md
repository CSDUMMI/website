---
author: CSDUMMI
---

Hi, I'm CSDUMMI (a.k.a. Joris Gutjahr). I'm a software developer.

[Mastodon](https://babka.social/@csdummi) [Email](mailto:me@jorisgutjahr.eu)
